import pandas as pd
from calculations import generate_correlation
from calculations import generate_df
from calculations import average_analysis
import matplotlib.pyplot as plt


def plot_drawing(number, input_df, moon_df, name):
    input_df['Timestamp'] = pd.to_datetime(input_df.iloc[:, 0])
    average_value = input_df.iloc[:, 7].mean()

    final_df = generate_df(input_df, moon_df)

    print(f'{name} results:')
    generate_correlation(final_df)

    below, over = average_analysis(final_df, average_value)
    print(f'Change value below average count: {below}')
    print(f'Change value over average count : {over}')
    print()

    plt.figure(number, figsize=(15, 10))
    plt.plot(final_df['Timestamp'], final_df['FullMoon'], label='Moon Phase Indicator')
    plt.plot(final_df['Timestamp'], final_df['ChangeValue'], label=f'{name} value change in points')
    plt.xlabel('Date')
    plt.ylabel(f'Indices - {name} price change in points')
    plt.title(f'{name} / Moon Phase Correlation')

    for date in moon_df['FullMoonDate']:
        plt.axvline(x=date, color='gray', linestyle='--', alpha=0.7)

    plt.axhline(y=average_value, color='red', linestyle="-",
            label=f'Average {name} change value = {average_value:.2f}')

    plt.legend()
