import pandas as pd
import matplotlib.pyplot as plt
from draw_plot import plot_drawing
import threading

moon_df = pd.read_excel("data.ods", engine="odf", sheet_name="MoonPhases")
moon_df['FullMoonDate'] = pd.to_datetime(moon_df.iloc[:, 0])

def read_dax_data():
    global dax_df
    dax_df = pd.read_excel("data.ods", engine="odf", sheet_name="Dax")

def read_nasdaq_data():
    global nasdaq_df
    nasdaq_df = pd.read_excel("data.ods", engine="odf", sheet_name="Nasdaq")


thread_dax = threading.Thread(target=read_dax_data())
thread_nasdaq = threading.Thread(target=read_nasdaq_data())

thread_dax.start()
thread_nasdaq.start()

thread_dax.join()
thread_nasdaq.join()

plot_drawing(1, dax_df, moon_df, 'DAX')
plot_drawing(2, nasdaq_df, moon_df, 'NASDAQ')

plt.show()
