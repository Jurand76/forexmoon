# forexMoon

Data analysis for real stock market.

Checking whether there is a correlation between:
- the phases of the moon 
- the strength of stock market index movements. 

There is a myth that during a full moon investors behave least rationally 
and for this reason the stock market "wobbles". 