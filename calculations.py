import pandas as pd
from scipy.stats import spearmanr

def strength(correlation):
    if -.1 < correlation < .1:
        return ('No correlation')
    if -.3 < correlation < .3:
        return ('Weak correlation')
    if -.6 < correlation < .6:
        return ('Medium correlation')
    if -.8 < correlation < .8:
        return ('Strong correlation')


def generate_df(input_df, moon_df):
    date_range = pd.date_range(start=input_df['Timestamp'].min(), end=input_df['Timestamp'].max())
    merged_df = pd.DataFrame({'Timestamp': date_range})
    merged_df['FullMoon'] = 0

    # Mark full moon days
    for date in moon_df['FullMoonDate']:
        merged_df.loc[merged_df['Timestamp'] == date, 'FullMoon'] = 30

    # Calculate days exactly between full moons
    for i in range(len(moon_df) - 1):
        start = moon_df['FullMoonDate'].iloc[i]
        end = moon_df['FullMoonDate'].iloc[i + 1]
        half_way = start + (end - start) / 2
        total_days = int((end - start).days)

        for j in range(total_days):
            current_date = start + pd.Timedelta(days=j)

            # Linear value assignment
            if current_date < half_way:
                # increasing linearly from 0 to 30
                value = 30 - (30 / total_days) * j * 2
            if current_date > half_way:
                # decreasing from 30 to 0
                value = (j - total_days / 2) * (30 / total_days) * 2
            if current_date == half_way:
                # date the same like date of full moon
                value = 30

            merged_df.loc[merged_df['Timestamp'] == current_date, 'FullMoon'] = value

    merged_df2 = pd.DataFrame({'Timestamp': input_df['Timestamp'], 'ChangeValue': input_df.iloc[:, 7]})
    merged_df2 = merged_df2.merge(merged_df[['Timestamp', 'FullMoon']], on='Timestamp', how='left')

    return merged_df2

def average_analysis(input_df, average):
    belowAverage = 0
    overAverage = 0
    for i in range(len(input_df)):
        if input_df['FullMoon'].iloc[i] == 30:
            if input_df['ChangeValue'].iloc[i] >= average:
                overAverage += 1
            else:
                belowAverage += 1

    return belowAverage, overAverage

def generate_correlation(input_df):
    correlation_pearson = input_df['FullMoon'].corr(input_df['ChangeValue'], method='pearson')
    print(f"Pearson correlation  : {correlation_pearson:4.2} - {strength(correlation_pearson)}")

    correlation_spearman1, p_value = spearmanr(input_df['FullMoon'], input_df['ChangeValue'])
    print(f"Spearman1 correlation: {correlation_spearman1:4.2} - {strength(correlation_spearman1)}")

    correlation_spearman2 = input_df['FullMoon'].corr(input_df['ChangeValue'], method='spearman')
    print(f"Spearman2 correlation: {correlation_spearman2:4.2} - {strength(correlation_spearman2)}")

    correlation_kendall = input_df['FullMoon'].corr(input_df['ChangeValue'], method='kendall')
    print(f"Kendall correlation  : {correlation_kendall:4.2} - {strength(correlation_kendall)}")
